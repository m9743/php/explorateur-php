<article>
    <div class="mb-10 flex justify-center text-6xl">
        <h1>Explorer</h1>
    </div>

    <div class="container">
        <?php $path = "/var/www"; ?>
        
        <?php function displayTree($path) { ?>
            <?php $repertory = scandir($path); 
                unset($repertory[array_search(".", $repertory)]);
                unset($repertory[array_search("..", $repertory)]);
            ?>

            <?php foreach($repertory as $file): ?>

                <div class="ml-20">
                    <div class="mb-1 flex flex-row items-center">

                        <?php if(is_dir($path."/".$file)): ?>
                            <img id="folder" class="mr-5 icon" src="https://upload.wikimedia.org/wikipedia/commons/e/e5/Circle-icons-folder.svg" alt="icone">
                            <p class="font-bold"><?= $file; ?></p>

                        <?php elseif(explode(".", $path."/".$file)[1] == "php"): ?>
                            <img class="mr-5 icon" src="https://upload.wikimedia.org/wikipedia/commons/2/27/PHP-logo.svg" alt="icone">
                            <p><?= $file; ?></p>

                        <?php elseif(explode(".", $path."/".$file)[1] == "css"): ?>
                            <img class="mr-5 icon" src="https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg" alt="icone">
                            <p><?= $file; ?></p>

                        <?php else: ?>
                            <img class="mr-5 icon" src="https://upload.wikimedia.org/wikipedia/commons/2/20/Text-x-generic.svg" alt="icone">
                            <p><?= $file; ?></p>

                        <?php endif; ?>
                    
                    
                        

                    </div>

                    <?php 
                        if(is_dir($path."/".$file)) {
                            displayTree($path."/".$file);
                        }
                    ?>
                </div>
                
                
            <?php endforeach ?>
        <?php } ?>
        
        <?php displayTree($path) ?>

    </div>

</article>